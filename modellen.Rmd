# Modellen {#modellen}

```{r modellen-colofon, eval=FALSE, echo=FALSE, results="asis"}

md4e::knitWithClass(
  
  "Auteur: Peter Verboon; laatste update: 2021-04-14",

  class="colofon"
)

```

```{r modellen-summary, echo=FALSE, results="asis"}

md4e::knitListWithClass(
  
  prefix="In dit hoofdstuk wordt besproken:",
  
  "Conceptuele modellen",
  "Meetmodellen en structurele modellen",
  "DAGs",

  class="overview"
)

```

```{r modellen-courses, echo=FALSE, results="asis"}

md4e::knitListWithClass(
  
  prefix="Deze stof wordt behandeld in de volgende Open Universiteitscursus(sen):",
  
  "Onderzoekspractium cross-sectioneel onderzoek (PB0812)",

  class="courses"
)

```

```{r modellen-dependencies, echo=FALSE, results="asis"}

md4e::knitListWithClass(
  
  prefix="Dit hoofdstuk bouwt voort op deze andere hoofdstukken:",
  
  "Wetenschap",
  "Psychologie",
  "Constructen",
  
  class="dependencies"
)

```

## Inleiding

Wetenschappelijke kennis wordt vaak gerepresenteerd als een model. Een model is een visuele weergave van een verzameling aannames. Dergelijke visualisatie is een vorm van formalisering: door een narratieve theorie in een model uit te drukken, is het duidelijker wat de implicaties zijn en hoe dus kan worden bepaald hoe plausibel die theorie is.

Helaas zijn modellen ook erg bruikbaar in de wiskunde en de statistiek. De modellen die daar worden opgesteld zien er vaak hetzelfde uit als modellen die een (deel van) een theorie beschrijven, maar de betekenis van de onderdelen van de twee typen modellen is fundamenteel verschillend. Het is daarom belangrijk om altijd voor jezelf in de gaten te houden of een bepaald model een theoretisch model is, of een statistisch model: beschrijft het, onder een serie aannames, patronen in de data? Dan is het een statistisch model. Beschrijft het hoe een deel van de natuurlijke wereld werkt? Dan is het een theoretisch model. Er bestaan ook modellen die een mix van deze twee zijn.

## Bouwstenen van een model

Een model bestaat in de basis uit zogenaamde knooppunten ("nodes") en takken ("edges"). De knooppunten geven concepten weer en de takken relaties tussen die concepten. De concepten kunnen van alles zijn: dat hangt af van het soort model. Zo'n knooppunt kan bijvoorbeeld staan voor depressie, een vraag in een vragenlijst, of het kijken van een film. Die relaties kunnen ook van alles zijn: ze kunnen structureel zijn (een tafel bestaat uit vier tafelpoten), causaal (lichaamsbeweging veroorzaakt zweten), of simpelweg een verband representeren (als er meer ijsjes worden gegeten, zijn er meer verdrinkingen).

## Soorten modellen

### Conceptuele modellen (theoretisch)

Theoretische modellen worden conceptuele modellen genoemd. Deze bevatten alleen theoretische constructen (zie hoofdstuk [Constructen](https://openmens.nl/constructen), hoofdstuk \@ref(constructen) in deze versie van het boek), en er zijn geen regels over hoe zulke modellen worden getekend.^[Hoewel er in 2019 een voorstel is gedaan voor [een formeel systeem voor conceptuele modellen](https://discovery.ucl.ac.uk/id/eprint/10077414/).]

Hoewel er geen formeel kader bestaat, zijn er wel twee conventies die bijna altijd worden gevolgd. Die hebben betrekking op de betekenis van pijlpunten aan het uiteinde van takken. Ten eerste drukt een tak met een pijlpunt aan maar één uiteinde bijna altijd de theoretische verwachting uit dat er een causaal verband is. Ten tweede drukt een tak met pijlpunten aan beide uiteinden bijna altijd de theoretische verwachting uit dat er verband is zonder dat de theorie stelt dat het causaal is. Dit laatste komt beduidend minder voor dan de eerste: bijna alle conceptuele modellen hebben alleen 'eenrichtingspijlen'.

Een voorbeeld van een simpel conceptueel model is de representatie van de zogenaamde Beredeneerd Gedrag Theorie. Die theorie stelt dat beredeneerd gedrag wordt bepaald door de intentie van mensen, en dat die intentie wordt bepaald door hun attitudes over dat gedrag en hun waargenomen normen met betrekking tot dat gedrag. Deze theorie kan worden gerepresenteerd in een model zoals getoond in figuur \@ref(fig:modellen-tra). Omdat er geen formele regels bestaan, hadden de rechthoekjes die de constructen representeren net zo goed ovalen kunnen zijn. Wel is het redelijk eenduidig dat hier drie causale relaties worden beschreven.

```{dot modellen-tra, fig.cap="Voorbeeldmodel: de Beredeneerd Gedrag Theorie.", fig.ext=ifelse(knitr::is_latex_output(), "pdf", "svg"), out.width="60%", echo=FALSE}

### Running this chunk requires GraphViz to be locally installed!
### See https://rdrr.io/cran/ndtv/man/install.graphviz.html
###
### This can conveniently be tested online in realtime at
### https://dreampuf.github.io/GraphvizOnline

digraph {
  
  rankdir=LR;
  
  Node      [shape=box, fontname="Arial"];

  Attitude  -> Intentie;
  Normen    -> Intentie;
  Intentie  -> Gedrag;

}

```

### Structurele modellen (statistisch)

Structurele modellen zijn statistische modellen. Ze drukken een rekenkundig model uit. Vaak lijkt een conceptueel model op het structurele model waarmee de theorie die met dat conceptuele model wordt uitgedrukt wordt onderzocht, maar lang niet altijd. Bovendien verschillen de modellen fundamenteel. Dit komt omdat een conceptueel model een model is van hoe een klein stukje werkelijkheid werkt (volgens de betreffende theorie, tenminste), terwijl een structureel model niet pretendeert iets over de werkelijkheid te zeggen: dat model is slechts een visuele weergave van een wiskundig model. Dit onderscheid is erg belangrijk in de psychologische wetenschap, omdat als je per abuis een structureel model interpreteert als een conceptueel model, je makkelijk conclusies trekt die niet kloppen.

Omdat structurele modellen visuele weergaven zijn van achterliggende wiskundige modellen, gelden hiervoor meer formele regels dan voor conceptuele modellen. Een knooppunt representeert nu ofwel een construct, ofwel een direct geobserveerde variabele (bijvoorbeeld een antwoord op een vraag). De naam ervan wordt in boxje van het knooppunt gezet. Je kunt herkennen wat een knooppunt representeert, want een construct wordt met een cirkel of ovaal weergegeven en een geobserveerde variabele met een rechthoek of vierkant. 

Er zijn drie belangrijke en veel voorkomende typen takken: een lijn zonder pijlpunten, een lijn met een pijlpunt aan één uiteinde en een lijn met pijlpunten aan beide uiteinden. Een lijn tussen twee knooppunten symboliseert een correlationeel verband tussen de knooppunten, dus tussen twee constructen of variabelen. Er is bij een lijn geen sprake van causaliteit: er wordt alleen uitgedrukt dat het wiskundig model aan de achterkant toestaat dat die twee knooppunten correleren.

Een enkele pijlpunt geeft aan dat er sprake is van causaliteit: het ene construct/variabele veroorzaakt het andere, en wel in de richting van de pijl. In het wiskundige model heeft dat bijvoorbeeld implicaties voor hoe de meetfout wordt berekend. Het is belangrijk om dit uitgedrukte "eenrichtingsverband" niet te verwarren met de betekenis van een causaal verband in een conceptueel model. Het is goed mogelijk om een structureel model te specificeren waarin causale verbanden staan die duidelijk onzin zijn, zonder dat de analyse uitwijst dat dat onzin is. Hier wordt uitgebreider op ingegaan in hoofdstuk [Causaliteit](https://openmens.nl/causaliteit) (hoofdstuk \@ref(causaliteit) in deze versie van het boek).

Tot slot wordt bij twee pijlen verondersteld dat de causaliteit beide kanten op werkt. De constructen hebben dan een effect op elkaar. Dit komt minder vaak voor dan de eerste twee.

In figuur \@ref(fig:modellen-structurele-voorbeelden) worden drie eenvoudige verbanden getoond, oftewel drie structurele modellen. Het eerste verband tussen `werkdruk` en `burnout` is een causaal verband waarbij wordt verondersteld dat het construct `werkdruk` het construct `burnout` veroorzaakt of tenminste een effect op `burnout` heeft.

```{dot modellen-structurele-voorbeelden, fig.cap="Drie voorbeelden van structurele modellen.", fig.ext=ifelse(knitr::is_latex_output(), "pdf", "svg"), out.width="50%", echo=FALSE}

digraph {
  
  rankdir=LR;
  
  Angst[shape=circle]
  Levensgeluk[shape=circle]
  Opleidingsniveau[shape=square]
  Vermijdingsgedrag[shape=square]
  

  Werkdruk -> Burnout
  
  concentrate = TRUE
  Angst -> Vermijdingsgedrag
  Vermijdingsgedrag -> Angst 
  
  edge[dir=none]
  Opleidingsniveau -> Levensgeluk 
  
  
}

```

Het tweede model relateert de geobserveerde variabele `opleidingsniveau` aan het het construct `levensgeluk`. Er wordt hier een samenhang veronderstelt, maar geen causaal verband. Het derde model geeft aan dat het construct `angst` (bijvoorbeeld de angst die iemand ervaart voor sociale contacten) een effect heeft op de geobserveerde variabele `vermijdingsgedrag`, maar dat tevens `vermijdingsgedrag` een effect heeft op `angst`. Het veronderstelde causale verband werkt hier dus twee kanten op.

### Meetmodellen

Meetmodellen representeren hoe een meetinstrument werkt. Dit type modellen heeft een eigen hoofdstuk: hoofdstuk [Constructen Meten](https://openmens.nl/constructen-meten) (hoofdstuk \@ref(constructen-meten) in deze versie van het boek).

### DAGs: directed acylic graphs

Een ander type conceptueel model is bekend onder de naam Directed Acyclic Graphs (DAGs). Een DAG is afkomstig uit de wiskunde en wordt veel gebruikt in velden waar experimenten moeilijk zijn. Als een wetenschapper geen experiment kan doen, is het opstellen van een uitgebreide DAG bijna de enige manier om toch conclusies te kunnen trekken over causaliteit. DAGs worden bijvoorbeeld veel gebruikt in de epidemiologie en in de financiele wereld. Bovendien zijn sommige cryptocurrencies gebaseerd op DAGs. 
Het doel is om causale processen te modelleren, vandaar de term "directed". Bij causaliteit loopt het proces altijd in een bepaalde richting namelijk van oorzaak naar gevolg. Dit vereist een heel nauwkeurige formulering van het model, waarin alle factoren die bij het causale proces zijn betrokken, moeten worden meegenomen. Veel werk met betrekking tot causaliteit is gedaan door Judea Pearl [@pearl_causality_2009].

Binnen de psychologie is het heel uitdagend om een adequate DAG op te stellen, omdat er meestal veel vaak onbekende factoren invloed hebben op de uitkomst en dus niet gemodelleerd kunnen worden. Maar indien er wel een correcte DAG gemaakt kan worden, dan kunnen er ook met niet-experimentele designs causale uitspraken worden gedaan. Dit komt verder aan bod in hoofdstuk [Causaliteit](https://openmens.nl/causaliteit) (hoofdstuk \@ref(causaliteit) in deze versie van het boek).
