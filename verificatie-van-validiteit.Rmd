# Verificatie van validiteit {#verificatie-van-validiteit}


```{r verificatie-van-validiteit-colofon, eval=FALSE, echo=FALSE, results="asis"}

md4e::knitWithClass(
  
  "Auteur: Gjalt-Jorn Peters; laatste update: 2020-08-18",

  class="colofon"
)

```

```{r verificatie-van-validiteit-summary, echo=FALSE, results="asis"}

md4e::knitListWithClass(
  
  prefix="In dit hoofdstuk wordt besproken:",
  
  "Ontologie en epistemologie",
  "Empirie en theorie",
  "Fundamentaal en toegepast onderzoek",
  "Technologie",

  class="overview"
)

```

```{r verificatie-van-validiteit-courses, echo=FALSE, results="asis"}

md4e::knitListWithClass(
  
  prefix="Deze stof wordt behandeld in de volgende Open Universiteitscursus(sen):",
  
  "Onderzoekspracticum cross-sectioneel onderzoek (PB0812)",
  
  class="courses"
)

```

```{r verificatie-van-validiteit-dependencies, echo=FALSE, results="asis"}

md4e::knitListWithClass(
  
  prefix="Dit hoofdstuk bouwt voort op de volgende hoofdstukken:",
  
  "Constructen",
  "Constructen Meten",
  "Interne Validiteit",
  "Validiteit Schatten en Verhogen",
  
  class="dependencies"
)

```

## De validiteit van een meetinstrument verifiëren

Zoals besproken in de hoofdstukken [Constructen](https://openmens.nl/constructen), [Constructen Meten](https://openmens.nl/constructen-meten), en [Validiteit van Meetinstrumenten](https://openmens.nl/validiteit-van-meetinstrumenten) vereist nadenken over validiteit dat je eerst de volgende zaken helder hebt:

- Wat is precies de definitie van het construct?
    - Welke aspecten van de psychologie omvat het construct precies?
    - Welke aspecten van de psychologie omvat het construct *niet*?
- Welke operationalisatie van het construct meet dit meetinstrument?
- Wat zijn de responsmodellen bij de items?

Dat responsmodel beschrijft hoe het doelconstruct de door het meetinstrument geregistreerde responsen veroorzaakt: de causale kettingen van het betreffende construct naar de responsen die het meetinstrument registreert. Op basis van dat responsmodel en het onderzoek tijdens de ontwikkeling van het meetinstrument zijn er daarna als het goed is verwachtingen over de volgende dingen:

- De verwachte verdeling van de responsen voor elk item;
- De verwachte verbanden tussen de items;
- De verwachte verbanden van de items met de datareeksen van de meetinstrumenten voor andere constructen
- De verwachte verbanden tussen de geaggregeerde scores (i.e. de score voor het gehele meetinstrument) en de datareeksen van de meetinstrumenten voor andere constructen

Als er geen responsmodellen beschikbaar zijn, bijvoorbeeld omdat het meetinstrument nog niet grondig is gevalideerd, volgen die verwachtingen dus ook niet uit de responsmodellen. In dat geval moet je hopen dat tijdens de validatie van het meetinstrument de volgende zaken goed zijn gedocumenteerd:

- De geobserveerde verdeling van de responsen voor elk item;
- De geobserveerde verbanden tussen de items;
- De geobserveerde verbanden van de items met de datareeksen van de meetinstrumenten voor andere constructen
- De geobserveerde verbanden tussen de geaggregeerde scores (i.e. de score voor het gehele meetinstrument) en de datareeksen van de meetinstrumenten voor andere constructen

Op basis van ofwel de voorspellingen van het responsmodel, ofwel de geobserveerde verdelingen en verbanden in de steekproeven die zijn gebruikt bij de ontwikkeling en validatie van het meetinstrument, kunnen vervolgens de instructies worden toegepast die de onderzoekers die het meetinstrument ontwikkelden of valideerden hebben gespecificeerd (zie paragrafen "Responspatronen", "Verbanden tussen items en interne consistentie", en "Convergentie en divergentie" in hoofdstuk "[Validiteit van Meetinstrumenten](https://openmens.nl/validiteit-van-meetinstrumenten)").

Als dergelijke instructies niet beschikbaar zijn, is het niet goed mogelijk om te bepalen of de manier waarop een meetinstrument werkt in een gegeven populatie en context voldoende overeenkomst met de manier waarop het meetinstrument zou moeten werken. In dat geval rest slechts een poging om zelf op basis van eerder onderzoek instructies te formuleren en die toe te passen. Hiervoor is de onderzoeker dan aangewezen op wat is gerapporteerd door andere onderzoekers: zowel het onderzoek waarmee het meetinstrument is ontwikkeld en gevalideerd, als vervolgonderzoek in populaties en contexten die heel vergelijkbaar zijn met de populatie en context waarin het meetinstrument is ontwikkeld en gevalideerd.
