
# Anova voor herhaalde metingen {#anova-herhaalde-metingen}



```{r anova-herhaalde-metingen-colofon, eval = T, echo=FALSE, results="asis"}

md4e::knitWithClass(
  
  "Auteurs: Peter Verboon; laatste update: 2021-08-02",

  class="colofon"
)

```

```{r anova-herhaalde-metingen-summary, eval = T, echo=FALSE, results="asis"}

md4e::knitListWithClass(
  
  prefix="In dit hoofdstuk wordt besproken:",
  
  "One-way variantieanalyse",
  "Factoriele anova",

  class="overview"
)

```

```{r anova-herhaalde-metingen-courses, eval = T, echo=FALSE, results="asis"}

md4e::knitListWithClass(
  
  prefix="Deze stof wordt behandeld in de volgende Open Universiteitscursus(sen):",
  
   "Onderzoekspractium experimenteel onderzoek (PB0412)",

  class="courses"
)

```

```{r anova-herhaalde-metingen-dependencies, eval = F, echo=FALSE, results="asis"}

md4e::knitListWithClass(
  
  prefix="Dit hoofdstuk bouwt voort op deze andere hoofdstukken:",
  
  "anova",
  "factoriele-anova",

  class="dependencies"
)

```



## Inleiding

Dit hoofdstuk bespreekt de situatie waarin een experimentele manipulatie niet (alleen) tussen personen maar ook binnen personen plaatvindt. Factoriele anova en oneway anova zijn gebaseerd op zogenaamde factoriele "between-subjects designs". De personen worden willekeurig aan één van de elkaar uitsluitende condities toegedeeld. Bij "within-subjects designs" worden personen aan meerdere experimentele condities blootgesteld, waarbij telkens de afhankelijke variabele wordt gemeten. Omdat dit niet tegelijkertijd kan, vindt de blootstelling aan de condities en metingen na elkaar plaats, vandaar de naam herhaalde metingen. De within subjects and between subjects designs kunnen ook worden gecombineerd: dit worden dan "mixed designs" genoemd.

In dit boek kiezen we ervoor om dit type modellen te bespreken en analyseren met behulp van het multilevel model, zie hoofdstukken (#multilevel-analyse) en (#multilevel-analyse-longitudinaal). 



## Herhaalde metingen design

In hoofdstuk #anova is de oneway anova besproken, een design met één numerieke afhankelijke variabele en één nominale predictor variabele. In hoofdstuk #factoriele-anova bestond het design uit één numerieke afhankelijke variabele en twee of meer nominale predictor variabelen. In dit hoofdstuk bestaat het design uit twee of meer afhankelijke variabelen en zijn (nominale) predictor variabelen geen vereiste. Eigenlijk zijn er geen meerdere afhankelijke variabelen, maar wordt dezelfde afhankelijke variabelen meerdere keren gemeten, bijvoorbeeld een keer voor een interventie of experimentele manipulatie, een keer na de interventie en een keer na een maand: herhaalde metingen van hetzelfde construct dus. 
Het voordeel van een within subjects designs is dat allerlei storende invloeden die verschillen tussen personen veroorzaken, onder controle worden gehouden. Eventuele storende variabelen zoals leeftijd, een persoonlijkheidskenmerk of intelligentie zijn per definitie constant binnen een persoon en worden in een within subjects design dus onder controle gehouden, terwijl deze variabelen in between subjects designs alleen via randomisatie onder controle kunnen worden gehouden. een beperking van within subjects designs is dat niet manipulaties herhaaldelijk aan subjecten kunnen worden toegepast. Een voorbeeld is een interventie met verschillende psychotherapietherapievormen. Het is weliswaar mogelijk om de therapievorm na een aantal weken te wijzigen, maar het is waarschijnlijk dat de eerste therapie nog doorwerkt, zodat het heel lastig is het effect van verschillende therapievormen te onderscheiden.
Met een herhaalde metingen design wordt een fundamenteel andere vraag beantwoord dan met een factorieel design. Bij een herhaalde metingen design gaat het namelijk om veranderingen binnen een persoon, terwijl het bij een between subjects design gaat om verschillen tussen groepen personen.



## Voorbeeld data

In het hoofdstuk over oneway anova (hoofdstuk #anova), werden de slaapkwaliteit data gebruikt als voorbeeld. De afhankelijke variabele was hier de gerapporteerde slaapkwaliteit en er was één predictor, "activiteit", die bestond uit drie condities: lezen, sporten, controle (geen instructie). In hoofdstuk #factoriele-anova werd een tweede factor geintroduceerd: "drinks". Deze factor bestond uit twee categorieen (condities): koffie en water. De subjecten werden willkeurig toebedeeld aan één van deze condities, waarbij men of de hele avond een hoeveelheid koffie moest drinken, of enkel water.
   
   
In dit voorbeeld beschouwen we data waarbij we de variabele "drinks" niet als between subjects manipulatie opnemen, maar als een within subjects factor. De variabele "activeit" negeren we voor dit moment. Een deel van de personen dronk een week lang s'avonds alleen water, daarna een week lang koffie, en tenslotte een week lang een paar glazen alcohol. Na iedere week rapporteerden de respondenten hun slaapkwaliteit van de afgelopen week. Iedere persoon heeft dus drie scores op slaapkwaliteit, elke score behoort bij een bepaalde manipulatie (hier: wat men heeft gedronken de week voorafgaand aan het rapporteren).
Om mogeljke volgorde-effecten van de drank op te sporen zijn de deelnemers willekeurig aan twee mogelijke volgordes toebedeeld. Sommige deelnemers dronken de eerste week koffie, daarna een week water en daarna een week alcohol (c-w-a), en de andere groep deelnemers begon met een week alcohol, daarna water en tenslotte koffie (a-w-c).


```{r anova-herhaalde-metingen-data, eval = TRUE, echo=FALSE, message = FALSE}

## In deze chunk wordt een dataset gemaakt om te illustreren (cf. puppy data Field)

#scales::show_col(viridis::viridis(8));
lineColor = "#9FDA3A";
errorColor = "#365C8D";
pointColor = "black";

#set.seed(20210126)

## sleep quality
control <- c(3,2,1,3,4,1,1,4,3,1,2,3)
exercise <- c(5,2,2,4,4,3,1,4,2,5,5,2)
read <- c(4,7,5,3,5,6,3,4,4,3,6,7)
sleepQuality <- c(control, exercise, read) + 3
N <- length(sleepQuality)

sleepQualityData <- data.frame(subject = 1:N,
                                activity = as.factor(c(rep("control", length(control)), 
                                                       rep("exercise", length(exercise)), 
                                                       rep("read",length(read)))),
                                order = as.factor(rep(c("c-w-a","a-w-c"), length(0.5*N))))

sleepQualityData$sleepQuality_c <- sleepQuality - round(rnorm(n=36, mean = 2, sd = .5),0)
sleepQualityData$sleepQuality_w <- sleepQuality + (sleepQualityData$activity == "exercise")*1
sleepQualityData$sleepQuality_a <- sleepQuality - round(rnorm(n=36, mean = 1, sd = .5),0)

## make data from wide to long data
library(tidyverse)

sleepQualitylong <- pivot_longer(sleepQualityData,
                        cols = sleepQuality_c:sleepQuality_a,
                        names_to = c("condition"),
                        values_to = "sleepQuality")

sleepQualitylong$condition <- as.factor(sleepQualitylong$condition)
levels(sleepQualitylong$condition) <- c("alcohol","coffee","water")
sleepQualitylong <- within(sleepQualitylong, condition <- relevel(condition, ref = "water"))


```

In de volgende figuur zijn de data van 12 deelnemers getoond: de lijnen verbinden de datapunten van dezelfde deelnemer. De drie metingen zijn gelabeld met de interventie conditie die vooraf ging aan de desbetreffende meting (horizontale as).
De trend van de interventies is duidelijk te zien: in de week water drinken is de gerapporteerde slaapkwaliteit het hoogst, bij koffie meestal het laagst.


```{r anova-herhaalde-metingen-plot1, eval= TRUE, echo=FALSE, fig.cap="Voorbeeld herhaalde metingen."   } 

ggplot2::ggplot(data = subset(sleepQualitylong, subject %in% seq(from=1, to=36, by=3)), 
                ggplot2::aes(x=condition, y=sleepQuality)) + 
  ggplot2::geom_point() +
  ggplot2::geom_line(ggplot2::aes(group=subject, color=as.factor(subject))) +
  ggplot2::theme(legend.position = "none") +  
  ggplot2::geom_jitter(width = 0.1, height = 0.1)


```

Als we de data van dezelfde personen nogmaals plotten, maar nu gekleurd naar de twee volgordes waarin men de dranken heeft gedronken, zien we dat de volgorde geen systematisch effect lijkt te  hebben op de slaapkwaliteit.

```{r anova-herhaalde-metingen-plot2, eval= TRUE, echo=FALSE, fig.cap="Voorbeeld herhaalde metingen, gekleurd naar volgorde."  } 

ggplot2::ggplot(data = subset(sleepQualitylong, subject %in% seq(from=1, to=36, by=3)), 
                ggplot2::aes(x=condition, y=sleepQuality, color = order)) + 
  ggplot2::geom_point() +
  ggplot2::geom_jitter(width = 0.1, height = 0.1)


```


### Data in wide format versus long format

Bij herhaalde metingen designs staan de herhaalde metingen traditioneel als variabelen in het databestand. Iedere rij correspondeert met een subject en elke subject heeft maar één rij met data. De afhankelijke variabelen worden bijvoorbeeld genoemd: slaapkwaliteit_1, slaapkwaliteit_2, slaapkwaliteit_3, waabij meteen duidelijk wordt dat hetzelfde construct 3 keer is gemeten. Dit format waarin de data staan, wordt aangeduid met de term "wide format".
   
   
Dezelfde data kunnen ook in een zogenaamd "long format" worden geplaatst. Elke score op de afhankelijke variabele komt dan in een aparte rij in het bestand: de scores van een subject staan dan onder elkaar in plaats van naast elkaar. Bij drie metingen van de afhankelijke variabele wordt het (long) bestand dus 3 maal zo lang, maar is er nog maar één afhankelijke variabele. Elk subject heeft in dat geval drie rijen in het bestand. Alle subject specifieke gegevens, zoals geslacht, leeftijd en conditie worden simpelweg gekopieerd, want deze blijven gelijk in de rijen die bij een bepaald subject horen.
Het voordeel van data in long format met één afhankelijke variabele is dat deze eenvoudiger te analyseren zijn met regressie-analyse-achtige technieken. Wel moet rekening worden gehouden met de geneste structuur van dit soort data: metingen van dezelfde persoon zijn niet meer onafhankelijk van elkaar, wat een belangrijke aanname is bij regressie-analyse. Er zijn echter analyse technieken, die speciaal zijn gemaakt om dit soort geneste data te analyseren. Dit worden technieken voor multilevel analyse genoemd, zie hoofdstuk #multilevel-analyse.


## Mixed designs

We spreken van mixed designs als er zowel een between subjects factor aanwezig is, als een within subjects factor (herhaalde meting). In het voorbeeld van de slaapkwaliteit data, zouden we het experiment, zoals hierboven besproken nog kunnen uitbreiden met de variabele activiteit: een between subjects factor zoals besproken in hoofdstuk #oneway-anova. We krijgen dan een mixed design: de subjects worden random aan een van de experimentele condities (sporten, lezen of controle) van de factor "activiteit" toebedeeld en moeten daarnaast een bepaalde drank tot zich nemen, afhankelijk van de week men zit. Uiteindelijk hebben de subjecten in één van de condities van activiteit gezeten (between subjects), maar hebben ze alle condities van de factor "drank" meegemaakt (within subjects).

## Analyse van herhaalde metingen designs

Herhaaldelijke metingen (repeated measures) variantieanalyse (RM-ANOVA) wordt van oudsher gebruikt voor de analyse van onderzoeksopzetten met herhaalde metingen. Het schenden van de veronderstellingen van RM-ANOVA kan echter problematisch zijn. Multilevel analyse (MLA) wordt tegenwoordig vaak gebruikt voor analyses van herhaalde metingen omdat het een alternatieve benadering biedt voor het analyseren van dit type gegevens. Deze benadering kent verschillende voordelen ten opzichte van RM-ANOVA, die hieronder worden besproken. 

1. MLA heeft minder strikte veronderstellingen: MLA kan worden gebruikt als de veronderstellingen van constante varianties (homogeniteit van de varianties of homoscedasticiteit), constante covarianties (compound symmetry) of constante varianties van de verschilscores (sphericity) worden geschonden voor RM-ANOVA. MLA maakt modellering van de variantie-covariantiematrix van dergelijke data mogelijk; dus, in tegenstelling tot RM-ANOVA, zijn deze aannames niet noodzakelijk.

2. MLA maakt een hiërarchische structuur mogelijk: MLA kan worden gebruikt voor steekproef-procedures van een hogere orde (bijvoorbeeld, personen, die worden gemeten op verschillende dagen en diverse keren binnen een dag), terwijl RM-ANOVA zich beperkt tot het onderzoeken van steekproef-procedures op twee niveaus (personen, gemeten op verschillende tijdstippen). Met andere woorden, MLA kan herhaalde metingen analyseren binnen personen, binnen een derde niveau van analyse enz., terwijl RM-ANOVA beperkt is tot herhaalde metingen binnen personen.  

3. MLA kan ontbrekende gegevens verwerken: ontbrekende gegevens zijn toegestaan in MLA zonder extra complicaties te veroorzaken. Met RM-ANOVA moeten gegevens van proefpersonen worden uitgesloten als er één datapunt ontbreekt. Ontbrekende gegevens en pogingen om met ontbrekende gegevens om te gaan (bijvoorbeeld door de ontbrekende gegevens te imputeren, gebruikmakend van het gemiddelde van de niet-ontbrekende gegevens van de persoon), kunnen extra problemen met RM-ANOVA veroorzaken.  

4. MLA kan ook gegevens verwerken waarin er variatie is in de exacte timing van de gegevensverzameling. De tijd tussen twee herhaalde metingen is bijvoorbeeld niet altijd hetzelfde. Gegevens voor een longitudinale studie kunnen bijvoorbeeld proberen metingen te verzamelen op de leeftijd van 6 maanden, 9 maanden, 12 maanden en 15 maanden. Deelnemersbeschikbaarheid, feestdagen en andere planningsproblemen kunnen echter leiden tot variatie met betrekking tot wanneer gegevens feitelijk worden verzameld. Deze variabiliteit kan in MLA worden meegenomen in het voorspelmodel door "leeftijd" toe te voegen aan de regressievergelijking. Het is ook niet noodzakelijk dat de intervallen tussen de meetpunten gelijk zijn in MLA.

5. Zowel RM-ANOVA, als MLA veronderstellen dat de afhankelijke variabele continu is, en gemeten op een interval- of ratioschaal en dat de residuen normaal verdeeld zijn. Er zijn echter ook gegeneraliseerde lineaire modellen voor andere typen afhankelijke variabelen, zoals categorische, ordinale, discrete tellingen. Voor een van deze uitkomsten is ANOVA geen optie. Er is dus geen RM-ANOVA-equivalent voor “count”- of logistische regressiemodellen. MLA kan wel met dit soort data omgaan.

6. In tegenstelling tot MLA werkt herhaalde metingen ANOVA niet goed wanneer de onderzoeksopzet niet gebalanceerd is, een situatie die in de praktijk zeer vaak voorkomt. 

### Conclusie

Als het ontwerp heel eenvoudig is en er zijn geen ontbrekende gegevens, dan leveren RM-ANOVA en een MLA waarschijnlijk identieke resultaten op. Bijvoorbeeld, een pre-post  ontwerp (met slechts twee herhalingen) of een experiment met een enkele “between-subjects” factor en een enkele “within-subjects” factor. Als dat het geval is, is RM-ANOVA meestal prima. De flexibiliteit van MLA modellen wordt belangrijker naarmate het onderzoeksontwerp gecompliceerder is.

