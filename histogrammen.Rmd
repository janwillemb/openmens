---
output: html_document
editor_options: 
  chunk_output_type: console
---
# Histogrammen {#histogrammen}


```{r histogrammen-summary, echo=FALSE, results="asis"}

md4e::knitListWithClass(
  
  prefix="In dit hoofdstuk wordt besproken:",
  
  "Histogrammen",

  class="overview"
)

```

```{r histogrammen-courses, echo=FALSE, results="asis"}

md4e::knitListWithClass(
  
  prefix="Deze stof wordt behandeld in de volgende Open Universiteitscursus(sen):",
  
  "Onderzoekspractium inleiding data-analyse (PB0202)",

  class="courses"
)

```

```{r histogrammen-dependencies, echo=FALSE, results="asis"}

md4e::knitListWithClass(
  
  prefix="Dit hoofdstuk bouwt voort op deze andere hoofdstukken:",
  
  "Datasets",
  "Verdelingen",

  class="dependencies"
)

```

## Inleiding

Dit hoofdstuk wordt nog gevuld.
